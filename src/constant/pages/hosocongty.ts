import { insertCompanies, getCompanies,getDistinctPage, BrowserCrawler, ManageData } from '../../entity/models/index'
import { ICompanyRoot } from '../../entity/interfaces/index';
import { ICompanyData } from '../../entity/interfaces/index';
import * as common from '../common';
import { ImportType } from '../enum/importType';
import * as dotenv from 'dotenv'

dotenv.config()
// Định nghĩa kiểu dữ liệu cho đối tượng data

const timeout = Number(process.env.TIMEOUT)
const displayBrowser = process.env.DISPLAY_BROWSER === 'true' ? true : false
const fastLoad = process.env.FAST_LOAD === 'true' ? true : false
const profileName = String(process.env.PROFILE_NAME)
const profileName2 = String(process.env.PROFILE_NAME2)

const executablePath = String(process.env.EXECUTABLE_PATH)
const isUserAgent = process.env.IS_USER_AGENT === 'true' ? true : false

const mainPage = new BrowserCrawler(
    timeout,
    displayBrowser,
    fastLoad,
    profileName,
    executablePath,
    isUserAgent
);

const subPage = new BrowserCrawler(
    timeout,
    displayBrowser,
    fastLoad,
    profileName2,
    executablePath,
    isUserAgent
);


export async function CrawlHoSoCongTy() {
    const step1 = await crawlCompanyListings(common.MAIN_URL_HOSOCONGTY, mainPage);

    // if (step1) {
    //     const step2 = await crawlCompanyDetails(step1, subPage);
    //     console.log(JSON.stringify(step1))

    //     const manageData = new ManageData(step2)
    //     manageData.ExportJson('hosocongty.json')

    // }
}
export async function CrawlDetailHoSoCongTy(): Promise<boolean> {
    // const step2 = await getCompanies(1);
    const step1 = await getDistinctPage();
    console.log(step1)
    return true;

}
// Function to crawl company listings
async function crawlCompanyListings(mainUrl: string, mainPage: BrowserCrawler) {
    const companyInfoArray: Partial<ICompanyRoot>[] = [];
    const batchSize = 500; // Number of items to insert in each batch
    const PAGE_SIZE = 263881
    // Logic to crawl company listings...
    for (let index = 0; index < PAGE_SIZE; index++) {
        console.log(`Page index: ${index}`)
        const page = await mainPage.loadPage(`${common.MAIN_URL_HOSOCONGTY}page-${index}`);

        await page?.waitForSelector('.box_content .hsdn');
        // Lấy danh sách các thẻ li chứa thông tin công ty
        const companyElements = await page?.$$('.box_content .hsdn li');

        if (companyElements) {
            for (const element of companyElements) {
                const companyInfo = await page?.evaluate((li, index) => {
                    const mainUrl = 'https://hosocongty.vn/'

                    const titleElement = li.querySelector('h3 a');
                    const link = titleElement ? titleElement.getAttribute('href') : '';
                    const linkTake = link ? link : '';
                    const tencongty = titleElement ? titleElement.textContent!.trim() : '';
                    const homeLink = mainUrl;
                    const data: Partial<ICompanyRoot> = {
                        homeLink,
                        tencongty,
                        link: linkTake,
                        page: index
                    }

                    return data;
                }, element, index);

                // Đẩy thông tin vào mảng
                if (companyInfo) {
                    companyInfoArray.push(companyInfo);
                }
                if (companyInfoArray.length >= batchSize) {
                    // await insertCompanies(companyInfoArray);
                    console.log(`Print Data Page ${index} : ${JSON.stringify(companyInfoArray)}`)
                    companyInfoArray.length = 0; // Clear the array
                }

            }
            await mainPage.quit();

        }
        // const manageData = new ManageData(companyInfoArray);
        // manageData.ImportCrawlHoSoCongTyTable();
        await mainPage.quit();

    }
    return true;

}

async function crawlCompanyDetails(companyInfoArray: Partial<ICompanyRoot>[], subPage: BrowserCrawler) {
    const companyInfoDetailArray: Partial<ICompanyData>[] = [];
    if (companyInfoArray) {
        for (const companyInfo of companyInfoArray) {
            if (companyInfo) {
                if (companyInfo.link) {
                    // await pageDetail.goto(`${companyInfo.homeLink}${companyInfo.link}`);
                    const pageDetail = await subPage.loadPage(`${companyInfo.homeLink}${companyInfo.link}`);


                    // Lấy thông tin chi tiết của công ty bằng XPath
                    const companyDetails = await pageDetail?.evaluate(() => {
                        // Hàm hỗ trợ lấy element theo XPath
                        const xpathSelector = (xpath: any, element: any) => {
                            return document.evaluate(xpath, element, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                        };

                        // Khai báo biến lưu trữ thông tin
                        const data: Partial<ICompanyData> = {};
                        // Lấy thông tin mã số thuế
                        const taxNameElement = xpathSelector('//li/h1', document.body);
                        data.nameCompany = taxNameElement ? taxNameElement.textContent?.trim() : '';
                        // Lấy thông tin mã số thuế
                        const taxNameNationElement = xpathSelector('//li[label[contains(text(), "Tên quốc tế:")]]/span', document.body);
                        data.nameCompanyInternational = taxNameNationElement ? taxNameNationElement.textContent?.trim() : '';
                        // Lấy thông tin mã số thuế
                        const taxShortNameNationElement = xpathSelector('//li[label[contains(text(), "Tên viết tắt:")]]/span', document.body);
                        data.shortName = taxShortNameNationElement ? taxShortNameNationElement.textContent?.trim() : '';

                        // Lấy thông tin mã số thuế
                        const taxNumberElement = xpathSelector('//li[label[contains(text(), "Mã số thuế:")]]/span', document.body);
                        data.taxCode = taxNumberElement ? taxNumberElement.textContent?.trim() : '';

                        // Lấy thông tin địa chỉ thuế
                        const addressElement = xpathSelector('//li[label[contains(text(), "Địa chỉ thuế:")]]/span', document.body);
                        data.taxAddress = addressElement ? addressElement.textContent?.trim() : '';

                        // Lấy thông tin người đại diện pháp luật
                        const legalRepresentativeElement = xpathSelector('//li[label[contains(text(), "Đại diện pháp luật:")]]/span', document.body);
                        data.legalRepresentative = legalRepresentativeElement ? legalRepresentativeElement.textContent?.trim() : 'Không có thông tin';

                        // Lấy thông tin ngày cấp
                        const dateIssuedElement = xpathSelector('//li[label[contains(text(), "Ngày cấp:")]]/span', document.body);
                        data.issueDate = dateIssuedElement ? dateIssuedElement.textContent?.trim() : '';

                        // Lấy thông tin ngành nghề chính
                        const mainIndustryElement = xpathSelector('//li[label[contains(text(), "Ngành nghề chính:")]]/span', document.body);
                        data.businessMainLine = mainIndustryElement ? mainIndustryElement.textContent?.trim() : '';

                        // Lấy thông tin trạng thái
                        const statusElement = xpathSelector('//li[label[contains(text(), "Trạng thái:")]]/span', document.body);
                        const statusElementValue = statusElement ? statusElement.textContent?.trim() : '';
                        data.isActive = statusElementValue === 'Đang hoạt động' ? true : false;
                        // Lấy thông tin trạng thái
                        const emailElement = xpathSelector('//li[label[contains(text(), "Email:")]]/span', document.body);
                        const emailElementValue = emailElement ? emailElement.textContent?.trim() : '';
                        // data.specs?.push({ name: "Website", value: emailElementValue })
                        // Lấy thông tin trạng thái
                        const websiteElement = xpathSelector('//li[label[contains(text(), "Website:")]]/span', document.body);
                        const websiteElementValue = websiteElement ? websiteElement.textContent?.trim() : '';
                        // data.specs?.push({ name: "Email", value: websiteElementValue })

                        // Lấy text của thẻ <li> chứa thông tin về Điện thoại
                        const phoneLiElementsValue = null;
                        const phoneLiElements = document.querySelectorAll('li');
                        if (phoneLiElements) {
                            phoneLiElements.forEach(li => {
                                const labelElement = li.querySelector('label');
                                if (labelElement && labelElement.textContent?.trim() === 'Điện thoại:') {
                                    const phoneLiElementsValue = li.textContent?.trim();
                                    // data.specs?.push({ name: "Số điện thoại", value: phoneLiElementsValue })

                                }
                            });
                        }
                        else {
                            // data.specs?.push({ name: "Số điện thoại", value: phoneLiElementsValue })

                        }


                        // Lấy thuộc tính src của thẻ <img> nếu tồn tại
                        const imgElementValue = null;
                        const imgElement = document.querySelector('li img');
                        if (imgElement) {
                            const imgSrc = imgElement.getAttribute('src');
                            if (imgSrc) {
                                const imgElementValue = imgSrc;
                                // data.specs?.push({ name: "Ảnh số điện thoại", value: imgElementValue })

                            }
                        } else {
                            // data.specs?.push({ name: "Ảnh số điện thoại", value: imgElementValue })

                        }

                        // Lấy thông tin cập nhật lần cuối
                        const lastUpdatedElement = xpathSelector('//li[i[contains(@class, "fa-clock-o")]]', document.body);
                        const lastUpdatedElementValue = lastUpdatedElement ? lastUpdatedElement.textContent?.trim() : '';

                        // data.specs?.push({ name: "Cập nhật lần cuối vào", value: lastUpdatedElementValue })

                        // Select the elements containing business industry data
                        const businessIndustryElements = document.querySelectorAll('.hsdn.nnkd div:not(.category)');

                        // Map the elements to the desired format
                        data.businessActives = Array.from(businessIndustryElements).map(element => ({
                            code: element.querySelector('span')?.textContent?.trim() || '',
                            name: element.querySelector('a')?.textContent?.trim() || ''
                        }));

                        const tagElements = document.querySelectorAll('.hsdn li a');

                        data.tags = Array.from(tagElements).map(element => ({
                            slug: element.getAttribute('href') || '',
                            name: element.textContent?.trim() || '',
                            title: element.getAttribute('title') || ''

                        }));

                        data.sourceData = 'hosocongty'

                        return data;
                    });
                    companyInfoDetailArray.push(companyDetails as Partial<ICompanyData>);

                    // Đẩy thông tin vào mảng

                    await subPage.quit();
                }

            }
        }
        return companyInfoDetailArray;
    }




}

