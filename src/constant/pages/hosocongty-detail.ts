
import * as dotenv from 'dotenv'
import { load } from 'cheerio'
import axios, { AxiosError, AxiosProxyConfig } from 'axios';
import { link } from 'fs';
import cheerio from 'cheerio';
import { getDataByPageAync, insertHoSoCongTy } from '../../entity/models/index'
import { IBusinessActive, ICompanyData, ICompanyDataModel, ICompanyRoot, IInformationCompany, ITags } from '../../entity/interfaces';
dotenv.config()
import * as _ from "lodash";
import { getFreeHttpsProxy } from "../../entity/models/CrawlProxies";
import https from 'https';
import { gotScraping } from "got-scraping";

// Định nghĩa kiểu dữ liệu cho đối tượng data
import UserAgent from 'user-agents';

const timeout = Number(process.env.TIMEOUT)
const displayBrowser = process.env.DISPLAY_BROWSER === 'true' ? true : false
const fastLoad = process.env.FAST_LOAD === 'true' ? true : false
const profileName = String(process.env.PROFILE_NAME)
const profileName2 = String(process.env.PROFILE_NAME2)

const executablePath = String(process.env.EXECUTABLE_PATH)
const isUserAgent = process.env.IS_USER_AGENT === 'true' ? true : false
const userAgent = new UserAgent();

const PAGE_TOTAL = 263898;
import http from 'http';
import { time } from 'console';

// axios_instance.defaults.httpAgent.keepAlive = true;
// axios_instance.defaults.httpAgent.maxSockets = Infinity;
// axios_instance.defaults.headers.common['Accept-Encoding'] = 'gzip';
// axios_instance.defaults.timeout = 30000;
const MAIN_URL = 'https://hosocongty.vn/'

export async function CrawlHoSoCongTyNew(): Promise<boolean> {

    try {
        for (let index = 1; index < PAGE_TOTAL; index++) {
            let response;
            let axios_instance = await setupAxiosWithProxy();
            axios_instance.defaults.httpAgent.keepAlive = true;
            axios_instance.defaults.httpAgent.maxSockets = Infinity;
            axios_instance.defaults.headers.common['Accept-Encoding'] = 'gzip';
            axios_instance.defaults.timeout = 30000;
            response = await axios_instance.get(`https://hosocongty.vn/page-${index}`);
            if (response.status === 200) {
                const html = response.data;
                const $ = load(html);
                const companyPromises: any[] = [];

                $('.hsdn > li').each((_idx, element) => {
                    const $element = $(element);
                    const links = $element.find('a').map(function () {
                        return $(this).attr('href');
                    }).get();
                    const names = $element.find('a').map(function () {
                        return $(this).text();
                    }).get();
                    const address = $element.find('div').text()?.trim().split(':')[1] ?? '';

                    const company = {
                        link: links[0],
                        name: names[0],
                        masothue: names[1],
                        address
                    };
                    if (company !== undefined || company !== '') {
                        companyPromises.push(CrawlDetailHoSoCongTy(company, index));

                    }

                })
                await Promise.all(companyPromises);


            }
        }
    } catch (error) {
        console.error(`Loi Khi Crawk Data: ${error}`)
    }
    return true;

}
export async function CrawlHoSoCongTyNewConcurrency(): Promise<boolean> {

    try {
        for (let index = 32927; index <= 1427573; index++) {
            let timeRandom: number[] = [33,55,77,2222,1002,3030,4300, 1000, 100, 2000, 3000, 4000, 1100, 1200, 2200, 3300, 1500, 10, 20, 500, 900];
            const companyPromises: any = [];

            const companyEntities = await getDataByPageAync(index);

            // companyPromises.push(CrawlDetailHoSoCongTyConcurrency(companyEntities, index))
            let random = timeRandom[(Math.floor(Math.random() * timeRandom.length))];
            // Promise.all(companyPromises);
            if (companyEntities) {
                let pageNum = index;
                try {
                    await CrawlDetailHoSoCongTyConcurrency(companyEntities, pageNum)
                    await timeoutPageAync(random)


                } catch (error) {
                    console.error(error)
                }


            }
            await timeoutPageAync(random)
        }
    } catch (error) {
        console.error(`Loi Khi Crawk Data: ${error}`)
    }
    return true;

}






const CrawlDetailHoSoCongTy = async (company: any, index: number) => {
    try {

        if (company.link !== undefined) {
            const url = `${MAIN_URL}${company.link}`;
            let axios_instance = await setupAxiosWithProxy();
            axios_instance.defaults.httpAgent.keepAlive = true;
            axios_instance.defaults.httpAgent.maxSockets = Infinity;
            axios_instance.defaults.headers.common['Accept-Encoding'] = 'gzip';
            axios_instance.defaults.timeout = 30000;
            const response = await axios_instance.get(url);
            const $ = load(response.data);

            // Extracting information using CSS selectors
            const nameCompany = $('li h1').text();
            const nameInternational = getValueByLabel($, 'Tên quốc tế:');
            const shortName = getValueByLabel($, 'Tên viết tắt:');
            const taxCode = getValueByLabel($, 'Mã số thuế:');
            const taxAddress = getValueByLabel($, 'Địa chỉ thuế:');
            const phoneNumber = getPhoneNumber($); // Call the function to extract phone number
            const issueDate = getValueByLabel($, 'Ngày cấp:');
            const businessMainLine = getValueByLabel($, 'Ngành nghề chính:');
            const isActive = getValueByLabel($, 'Trạng thái:') === "Đang hoạt động" ? true : false;
            const lastUpdate = getValueByLabel($, 'Cập nhật lần cuối vào ');
            const legalRepresentative = getValueByLabel($, 'Đại diện pháp luật:');


            const companyRelatedData = getCompanyRelated($, 'Ngành nghề kinh doanh');

            const companyTags = getTags($, '')
            // Map companyTags
            const tagsMap: ITags[] = [];
            companyTags.forEach(tag => {
                const item: ITags = {
                    slug: tag.slug, name: tag.name, title: tag.title
                };
                tagsMap.push(item);
            });

            // Map companyRelatedData
            const businessActives: IBusinessActive[] = [];
            companyRelatedData.forEach(data => {
                const item: IBusinessActive = {
                    code: data.code, name: data.name
                };
                businessActives.push(item);
            });

            const companiesData: ICompanyData =
            {
                nameCompany,
                nameCompanyInternational: nameInternational,
                shortName,
                legalRepresentative,
                taxCode,
                taxAddress,
                phoneNumber,
                lastUpdate,
                issueDate,
                businessMainLine,
                isActive,
                businessActives,
                tags: companyTags,
                slug: company.link,
                sourceData: "hosocongty",
                page: index
            }

            // await insertCompanyDatasAsync(companiesData)

        } else {
        }
    } catch (error) {
        console.error('Error:', error);
    }
}

const CrawlDetailHoSoCongTyConcurrency = async (company: Partial<ICompanyRoot>[], index: number) => {
    try {
        let pageNumber = index;
        let axios_instance = await setupAxiosWithProxy();
        axios_instance.defaults.httpAgent.keepAlive = true;
        axios_instance.defaults.httpAgent.maxSockets = Infinity;
        // axios_instance.defaults.headers.common['Accept-Encoding'] = 'gzip';
        axios_instance.defaults.timeout = 5000;
        // axios.get('https://api.ipify.org/?format=json', {
        //     headers: { 'Content-Type': 'application/json', },
        //     proxy: {
        //         protocol: 'http',
        //         host: '3.35.51.228',
        //         port: 80
        //     }
        // }).then(response => {
        //     console.log(response.data);
        // });
        Promise.all(company.map((endpoint) => gotScraping.get(MAIN_URL.concat(endpoint.link ?? '')))).then(
            axios.spread(async (...allData) => {
                let insert: IInformationCompany[] = [];

                allData.forEach((response) => {
                    const $ = load(response.body);
                    // Extracting information using CSS selectors
                    const nameCompany = $('li h1').text();
                    const nameInternational = getValueByLabel($, 'Tên quốc tế:');
                    const shortName = getValueByLabel($, 'Tên viết tắt:');
                    const taxCode = getValueByLabel($, 'Mã số thuế:');
                    const taxAddress = getValueByLabel($, 'Địa chỉ thuế:');
                    const phoneNumber = getPhoneNumber($); // Call the function to extract phone number
                    const issueDate = getValueByLabel($, 'Ngày cấp:');
                    const businessMainLine = getValueByLabel($, 'Ngành nghề chính:');
                    const isActive = getValueByLabel($, 'Trạng thái:') === "Đang hoạt động" ? true : false;
                    const lastUpdate = getValueByLabel($, 'Cập nhật lần cuối vào ');
                    const legalRepresentative = getValueByLabel($, 'Đại diện pháp luật:');


                    const companyRelatedData = getCompanyRelated($, 'Ngành nghề kinh doanh');

                    const companyTags = getTags($, '')
                    // Map companyTags
                    const tagsMap: ITags[] = [];
                    companyTags.forEach(tag => {
                        const item: ITags = {
                            slug: tag.slug, name: tag.name, title: tag.title
                        };
                        tagsMap.push(item);
                    });

                    // Map companyRelatedData
                    const businessActives: IBusinessActive[] = [];
                    companyRelatedData.forEach(data => {
                        const item: IBusinessActive = {
                            code: data.code, name: data.name
                        };
                        businessActives.push(item);
                    });

                    const companiesData: ICompanyData =
                    {
                        nameCompany,
                        nameCompanyInternational: nameInternational,
                        shortName,
                        legalRepresentative,
                        taxCode,
                        taxAddress,
                        phoneNumber,
                        lastUpdate,
                        issueDate,
                        businessMainLine,
                        isActive,
                        businessActives,
                        tags: companyTags,
                        slug: '',
                        sourceData: "hosocongty",
                        page: index
                    }

                    const itemInsert: IInformationCompany = {
                        json: JSON.stringify(companiesData),
                        taxCode: companiesData.taxCode,
                        page: pageNumber
                    }

                    insert.push(itemInsert)
                })
                await SaveBatch(insert, pageNumber);
            })
        );

    } catch (error) {
        console.error('Error:', error);
    }
}
let batchInformationCompanyData: IInformationCompany[] = []; // Khởi tạo mảng rỗng

async function SaveBatch(input: IInformationCompany[], pageNumber: number) {
    const batchSize = 200;
    batchInformationCompanyData.push(...input); // Thêm dữ liệu vào mảng batchData

    console.log(`Processing.... page:${pageNumber} - total: ${batchInformationCompanyData.length}`);

    if (batchInformationCompanyData.length >= batchSize) {
        await insertHoSoCongTy(batchInformationCompanyData); // Chờ cho việc thêm dữ liệu vào cơ sở dữ liệu hoàn thành
        batchInformationCompanyData = []; // Sau khi thêm xong, reset lại mảng batchData
    }
}




// Helper function to get value by label
const getValueByLabel =
    ($: any, label: string) => {
        const liElement = $('li').filter((index: any, element: any) => {
            return $(element).find('label').text().trim() === label;
        });
        if (liElement.length > 0) {
            const value = liElement.find('span').text().trim();
            return value || 'N/A'; // Return 'N/A' if value is empty
        } else {
            return 'N/A'; // Return 'N/A' if liElement is not found
        }
    }

// Helper function to extract phone number
const getPhoneNumber = ($: any) => {
    const phoneNumberLi = $('li').filter((index: any, element: any) => {
        return $(element).find('label').text().trim() === 'Điện thoại:';
    });
    if (phoneNumberLi.length > 0) {
        const phoneNumberSpan: any = phoneNumberLi.find('span');
        const phoneNumberImg: any = phoneNumberSpan.find('img');
        if (phoneNumberImg.length > 0) {
            // Extract the phone number from the alt attribute of the <img> element
            const phoneNumber = phoneNumberImg.attr('src').trim();
            return phoneNumber || 'N/A'; // Return 'N/A' if phone number is empty
        } else {
            // Extract the text content of the <span> element
            const phoneNumber = phoneNumberSpan.text().trim();
            return phoneNumber || 'N/A'; // Return 'N/A' if phone number is empty
        }
    } else {
        return 'N/A'; // Return 'N/A' if phoneNumberLi is not found
    }
}
const getCompanyRelated = ($: any, label: string) => {
    const data: { code: string, name: string }[] = [];

    $('.nnkd > li > div > div').each((index: number, element: any) => {
        const code = $(element).find('span').text().trim();
        const name = $(element).find('a').text().trim();
        data.push({ code, name });
    });

    return data;
}

const getTags = ($: any, label: string) => {
    const data: { slug: string, name: string, title: string }[] = [];

    $('.hsdn > li > a').each((index: number, element: any) => {
        const slug = $(element).attr('href') || '';
        const name = $(element).text().trim();
        const title = $(element).attr('title') || '';
        data.push({ slug, name, title });
    });

    return data;
}

async function setupAxiosWithProxy() {
    let proxyConfig: AxiosProxyConfig = {
        host: '3.36.97.27',
        port: 51860,
        protocol: 'https'
    };

    const httpAgent = new https.Agent({
        keepAlive: true,
        maxSockets: Infinity,
        rejectUnauthorized: false

    });
    const userAgent = new UserAgent({ deviceCategory: 'desktop' });

    const axios_instance = axios.create({
        httpAgent: httpAgent,
        headers: {
            'Cookie': '',
            'User-Agent': `${userAgent}`,
            'Accept': "*/*",
        },
        // proxy: proxyConfig
    });

    // Add retry logic to the axios instance
    axios_instance.interceptors.response.use(undefined, (error: AxiosError) => {
        const { config, response } = error;
        const isAxiosError = error.isAxiosError || false;

        if (isAxiosError && response && response.status === 429) { // Retry if status code is 429 (Too Many Requests)
            return new Promise((resolve) => {
                if (config) {
                    setTimeout(() => resolve(axios_instance.request(config)), 10000); // Retry after 1 second
                }
            });

        }

        if (error.code === 'ECONNRESET') { // Retry if connection reset
            return new Promise((resolve) => {
                if (config) {
                    setTimeout(() => resolve(axios_instance.request(config)), 10000); // Retry after 1 second

                }
            });
        }

        return Promise.reject(error);
    });

    return axios_instance;
}
function timeoutPageAync(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
