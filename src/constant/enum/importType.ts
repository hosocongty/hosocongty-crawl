export enum ImportType {
    JSON,
    CSV,
    XLXS
}