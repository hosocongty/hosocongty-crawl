import * as dotenv from 'dotenv'
import * as rabbitMq from "../models/index";

dotenv.config()

export type INotification = {
  title: string;
  description: string;
};

export const sendNotification = async (notification: INotification) => {
  await rabbitMq.mqConnection.sendToQueue(process.env.RABBITMQ_NOTIFICATION_QUEUE ?? 'test', notification);

  console.log(`Sent the notification to consumer`);
};