import mongoose, { Schema, Document } from 'mongoose';


export interface IInformationCompany {
    json: string;
    taxCode: string;
    page: number;

}

export interface IInformationCompanyDataModel extends IInformationCompany, Document {}

const InformationCompanyDataSchema: Schema = new Schema({
    json: { type: String, required: false },
    taxCode: { type: String, required: false },
    page: { type: Number, required: false },
   

});

const InformationCompanyData = mongoose.model<IInformationCompanyDataModel>('InformationCompanyData', InformationCompanyDataSchema);

export default InformationCompanyData;