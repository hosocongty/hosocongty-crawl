import mongoose, { Schema, Document } from 'mongoose';
export interface IBusinessActive {
    code?: string;
    name?: string;
}
export interface ITags {
    slug: string;
    name: string;
    title: string;

}

export interface ICompanyData {
    nameCompany: string;
    nameCompanyInternational: string;
    shortName: string;
    legalRepresentative: string;
    taxCode: string;
    taxAddress: string;
    phoneNumber:string,
    lastUpdate:string,
    issueDate: string;
    businessMainLine: string;
    isActive: boolean;
    // specs: { name: string, value: any }[];
    businessActives:IBusinessActive[];
    tags: ITags[];
    slug: string;
    sourceData: string;
    page: number;

}

export interface ICompanyDataModel extends ICompanyData, Document {}

const CompanyDataSchema: Schema = new Schema({
    nameCompany: { type: String, required: false },
    nameCompanyInternational: { type: String, required: false },
    shortName: { type: String, required: false },
    legalRepresentative: { type: String, required: false },
    taxCode: { type: String, required: false },
    taxAddress: { type: String, required: false },
    phoneNumber:{ type: String, required: false },
    lastUpdate:{ type: String, required: false },
    issueDate: { type: String, required: false },
    businessMainLine: { type: String, required: false },
    isActive: { type: Boolean, required: false },
    // specs: [{ type: Map, of: Schema.Types.Mixed }], // Using Map for dynamic keys
    businessActives: [{ code: Schema.Types.Mixed, name: Schema.Types.Mixed }], // Array of Maps
    tags: [{ slug: Schema.Types.Mixed, name: Schema.Types.Mixed }], // Array of Maps
    slug: { type: String, required: false },
    sourceData: { type: String, required: false },
    page: { type: Number, required: false }

});

const CompanyData = mongoose.model<ICompanyDataModel>('CompanyData', CompanyDataSchema);

export default CompanyData;