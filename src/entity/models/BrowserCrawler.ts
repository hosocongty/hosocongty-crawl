// import puppeteer from 'puppeteer';
import puppeteerVanilla from 'puppeteer';
import { addExtra } from 'puppeteer-extra';
import UserAgent from 'user-agents';

// import  puppeteer from "puppeteer-extra";
import AdblockerPlugin from "puppeteer-extra-plugin-adblocker";
import StealthPlugin from 'puppeteer-extra-plugin-stealth';

import AcceptLanguagePlugin from 'puppeteer-extra-plugin-stealth';
import ChromeRuntimePlugin from 'puppeteer-extra-plugin-stealth';
import ConsoleDebugPlugin from 'puppeteer-extra-plugin-stealth';
import IFrameContentWindowPlugin from 'puppeteer-extra-plugin-stealth';
import MediaCodecsPlugin from 'puppeteer-extra-plugin-stealth';
import NavigatorLanguagesPlugin from 'puppeteer-extra-plugin-stealth';
import NavigatorPermissionsPlugin from 'puppeteer-extra-plugin-stealth';
import NavigatorPlugins from 'puppeteer-extra-plugin-stealth';
import WebdriverPlugin from 'puppeteer-extra-plugin-stealth';
import UserAgentPlugin from 'puppeteer-extra-plugin-stealth';
import WebglVendorPlugin from 'puppeteer-extra-plugin-stealth';
import WindowOuterDimensionsPlugin from 'puppeteer-extra-plugin-stealth';
import {  Page } from "puppeteer";
import * as fs from 'fs';
import * as path from 'path';
import { IProfile } from '../interfaces/index';
import { BrowserWrapper } from './BrowserWrapper'
export class BrowserCrawler {
    private _browser: BrowserWrapper;
    private _page: Page | null = null;
    private _hasError: boolean = false;

    constructor(private timeout: number, private displayBrowser: boolean,
        private fastLoad: boolean, private profileName: string,
        private executablePath: string,
        private isUserAgent: boolean
    ) {
        this._browser = new BrowserWrapper();
    }

    private async getProfile(): Promise<IProfile | null> {
        if (!this.profileName || this.profileName === '') return null;
        const profilePath = path.join('./', 'src', 'profiles', this.profileName);
        try {
            await fs.promises.access(profilePath);
            return { name: this.profileName, path: profilePath };
        } catch (err) {
            console.log(`IProfile ${this.profileName} doesn't exist yet`);
            console.log('You need to create the IProfile with setup_browser.py');
            console.log('Using default IProfile for this session');
            await fs.promises.mkdir(profilePath);
            return { name: this.profileName, path: profilePath };
        }
    }

    private async saveProfile(profileTempPath: string): Promise<boolean> {
        if (!this.profileName) return true;
        const profilePath = path.join('./', 'src', 'profiles', this.profileName);
        try {
            await fs.promises.copyFile(profileTempPath, profilePath);
            return true;
        } catch (err) {
            console.error(`Can't save IProfile ${this.profileName}`);
            return false;
        }
    }

    private async createBrowser() {
        const puppeteer = addExtra(puppeteerVanilla);
        puppeteer
            .use(StealthPlugin())
            .use(AdblockerPlugin({ blockTrackers: true }));

        const plugins = [
            ChromeRuntimePlugin(),
            IFrameContentWindowPlugin(),
            MediaCodecsPlugin(),
            NavigatorLanguagesPlugin(),
            NavigatorPermissionsPlugin(),
            NavigatorPlugins(),
            WebdriverPlugin(),
            WebglVendorPlugin(),
            WindowOuterDimensionsPlugin(),
            UserAgentPlugin(),
            AcceptLanguagePlugin(),
            ConsoleDebugPlugin()
        ];


        const args: string[] = ['--no-sandbox'];
        if (!this.displayBrowser) args.push('--headless');
        const profile = await this.getProfile();
        const browser = await puppeteer.launch(
            {
                args, executablePath: this.executablePath,
                headless: this.displayBrowser,
                userDataDir: profile ? profile.path : undefined
            }
        );
        for (const plugin of plugins) {
            await plugin.onBrowser(browser);
        }
        this._browser.setBrowser(browser);
        this._page = await browser.newPage()
        if (this.isUserAgent) {
            const userAgent = new UserAgent({ deviceCategory: 'desktop' });
            this._page.setUserAgent(userAgent.data.userAgent);

        }
        for (const plugin of plugins) {
            await plugin.onPageCreated(this._page);
        }

        // this._page = await browser.newPage();
        await this.setupPage();
    }

    private async setupPage() {
        if (!this._page) return;
        this._page.setDefaultNavigationTimeout(this.timeout);
        if (this.fastLoad) {
            await this._page.setBypassCSP(true);
            await this._page.setRequestInterception(true);
            this._page.on('request', (request) => {
                if (['image', 'stylesheet', 'font', 'media', 'texttrack', 'object', 'beacon', 'csp_report', 'imageset'].includes(request.resourceType())) {
                    request.abort();
                } else {
                    request.continue();
                }
            });
        }
    }


    async loadPage(url: string, preventAutoRedirect: boolean = false){
        if (!this._browser.getBrowser()) {
            await this.createBrowser();
        }
        if (!this._page) throw new Error();
        try {
            await this._page.goto(url);
            if (preventAutoRedirect) {
                await this._page.reload({ waitUntil: ['domcontentloaded', 'networkidle0'] });
            }
            return await this._page;
        } catch (err) {
            console.error(err);
            this._hasError = true;
        }
    }

    async getPageHTML(): Promise<string> {
        if (!this._page) return '';
        return await this._page.content();
    }
    async getPage(): Promise<Page> {
        if (this._page) {
            return await this._page;
        }
        throw new Error();
    }
    async quit() {
        if (this._browser.getBrowser()) {
            await this._browser.quit();
        }
    }

    hasError(): boolean {
        return this._hasError;
    }
}




