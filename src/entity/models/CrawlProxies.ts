import axios from 'axios';
import { load } from 'cheerio'; // Assuming you are using Cheerio for HTML parsing

interface ProxyInfo {
    host: string;
    port: number;
}

const checkProxy = async (proxy: ProxyInfo): Promise<boolean | any> => {
    try {
        console.log(proxy)
        axios.get('https://hosocongty.vn/', {
            proxy: {
                host: proxy.host,
                port: proxy.port,
                protocol: 'https'
            }
        }).then(response => {
            return response.status === 200 ? true : false;
        });
    } catch (error) {
        return false;
    }
};

export async function getFreeHttpsProxy(): Promise<ProxyInfo[]> {
    const proxyList: ProxyInfo[] = [];
    let proxyListRS: ProxyInfo[] = [];

    try {
        
        const response = await axios.get('https://www.sslproxies.org');
        const html = response.data;
        const $ = load(html);
        $('.table .table-striped .table-bordered tr').each((_idx: any, element: any) => {
            const tds = $(element).find('td');
            if (tds.length >= 2) {
                const host = tds.eq(0).text();
                const port = parseInt(tds.eq(1).text());
                const proxy: ProxyInfo = { host, port };
                proxyList.push(proxy);
            }
        });

        // Using Promise.all to wait for all checkProxy promises to resolve
        await Promise.all(proxyList.map(async (proxy) => {
            const isActive = await checkProxy(proxy);
            if (isActive) {
                proxyListRS.push(proxy);
            }
        }));
    } catch (error) {
        console.error('Error fetching proxy list:', error);
    }

    return proxyListRS;
}
