
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()
import { uuid } from 'uuidv4';
import { ICompanyRoot, IInformationCompany } from '../interfaces/index';

export const insertCompanies = async (companies: Partial<ICompanyRoot>[]) => {
    try {
        const companyData = companies.filter(item => item.link !== '' && item.link !== null).map(item => ({
            id: uuid(),
            homeLink: item.homeLink ?? '',
            link: item.link ?? '',
            nameCompany: item.tencongty ?? '',
            isCrawl: false,
            page: item.page ?? -1
        }));
        await prisma.crawlHoSoCongTy.createMany({ data: companyData });
        await prisma.$disconnect();
        console.log(`Inserted ${companyData.length} companies into CrawlHoSoCongTy table.`);
    } catch (error) {
        console.error('Error while inserting companies:', error);
    } finally {
        await prisma.$disconnect();
    }
};

export const insertHoSoCongTy = async (informationCompany: IInformationCompany[]) => {
    try {
       
        await prisma.hoSoCongTy.createMany({ data:  informationCompany});
        await prisma.$disconnect();
        console.log(`Inserted ${informationCompany.length} companies into HoSoCongTy table.`);
    } catch (error) {
        console.error('Error while inserting companies:', error);
    } finally {
        await prisma.$disconnect();
    }
};


export const getCompanies = async (pageIndex: number): Promise<Partial<ICompanyRoot>[] | null> => {
    try {
        const companies = await prisma.crawlHoSoCongTy.findMany({ where: { page: pageIndex } });
        await prisma.$disconnect();
        return await mapperGetCompanies(companies);
    } catch (error) {
        console.error('Error while inserting companies:', error);
        return null;
    }
}

export const getDistinctPage = async (): Promise<any | null> => {
    try {
        const companies = await prisma.$queryRaw`
        select distinct page  from "CrawlHoSoCongTy" chsct 
    `;
        await prisma.$disconnect();
        return companies;
    } catch (error) {
        console.error('Error while inserting companies:', error);
        return null;
    }
}




export const getDataByPageAync = async (pageNumber: number): Promise<Partial<ICompanyRoot>[] | null> => {
    try {
        //     const companies = await prisma.$queryRaw`
        //     select chsct.id ,chsct.link ,chsct."isCrawl"  from "CrawlHoSoCongTy" chsct where chsct.page =${pageNumber}
        // `;
        const companies = await prisma.crawlHoSoCongTy.findMany({ where: { page: pageNumber } });
        await prisma.$disconnect();
        return await mapperGetCompanies(companies);
    } catch (error) {
        console.error('Error while inserting companies:', error);
        return null;
    }
}

const mapperGetCompanies = async (data: any[]): Promise<Partial<ICompanyRoot>[] | null> => {
    const result: Partial<ICompanyRoot>[] = [];
    data.forEach(value => {
        const newData: Partial<ICompanyRoot> = {}
        newData.homeLink = value.homeLink;
        newData.link = value.link;
        newData.page = value.page;
        newData.isCrawl = value.isCrawl;
        result.push(newData);
    })

    return result;
}