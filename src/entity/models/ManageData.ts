
import * as fs from 'fs';
import { ImportType } from '../../constant/index'
import * as ExcelJS from 'exceljs';

export class ManageData {
    private _data: any[];

    constructor(data: any[]) {
        this._data = data;
    }


    public async ExportJson(filePath: string): Promise<boolean> {
        try {
            const jsonData = JSON.stringify(this._data);
            await fs.promises.writeFile(filePath, jsonData);
            console.log("Data exported successfully to JSON file:", filePath);
            return true;
        } catch (error) {
            console.error("Error exporting data to JSON:", error);
            return false;
        }
    }

    public async ExportCsv(filePath: string): Promise<boolean> {
        try {
            // Check if the data is an array
            if (!Array.isArray(this._data)) {
                console.error("Data is not in valid format for CSV export.");
                return false;
            }

            // Check if the array is empty
            if (this._data.length === 0) {
                console.error("Data array is empty.");
                return false;
            }

            // Extract headers from the first object in the array
            const headers = Object.keys(this._data[0]);

            // Generate CSV string
            let csvContent = headers.join(',') + '\n';
            csvContent += this._data.map((obj: any) => headers.map(header => obj[header]).join(',')).join('\n');

            // Write CSV content to file
            await fs.promises.writeFile(filePath, csvContent);
            console.log("Data exported successfully to CSV file:", filePath);
            return true;
        } catch (error) {
            console.error("Error exporting data to CSV:", error);
            return false;
        }
    }
    public async ExportXlsx(filePath: string): Promise<boolean> {
        try {
            // Create a new workbook
            const workbook = new ExcelJS.Workbook();

            // Add a worksheet
            const worksheet = workbook.addWorksheet('Data');

            // Check if the data is an array
            if (!Array.isArray(this._data)) {
                console.error("Data is not in valid format for Excel export.");
                return false;
            }

            // Check if the array is empty
            if (this._data.length === 0) {
                console.error("Data array is empty.");
                return false;
            }

            // Extract headers from the first object in the array
            const headers = Object.keys(this._data[0]);

            // Add headers to the worksheet
            worksheet.addRow(headers);

            // Add data rows to the worksheet
            this._data.forEach((row: any) => {
                const rowData = headers.map(header => row[header]);
                worksheet.addRow(rowData);
            });

            // Write workbook to file
            await workbook.xlsx.writeFile(filePath);

            console.log("Data exported successfully to Excel file:", filePath);
            return true;
        } catch (error) {
            console.error("Error exporting data to Excel:", error);
            return false;
        }
    }

    // public async ImportCrawlHoSoCongTyTable() {
    //     try {
    //         await Promise.all(this._data.map(async (item) => {
    //             if (item.link !== '' && item.link !== null) {
    //                 const { homeLink, link, tencongty, page } = item;
    //                 const indexPage = parseInt(page);
    //                 await prisma.crawlHoSoCongTy.create({
    //                     data: {
    //                         id:uuid(),
    //                         homeLink,
    //                         link,
    //                         nameCompany: tencongty,
    //                         isCrawl: false,
    //                         page: indexPage
    //                     }
    //                 });
    //             }
    //         }));
    
    //         console.log('Imported data into CrawlHoSoCongTy table successfully.');
    //     } catch (error) {
    //         console.error('Error while importing data into CrawlHoSoCongTy table:', error);
    //     } finally {
    //         await prisma.$disconnect();
    //     }
    // }

    
}
