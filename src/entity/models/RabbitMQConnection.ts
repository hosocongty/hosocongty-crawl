import client, { Connection, Channel, ConsumeMessage } from "amqplib";
import * as dotenv from 'dotenv'
dotenv.config()

type HandlerCB = (msg: string) => any;

class RabbitMQConnection {
  private connection!: Connection;
  private channel!: Channel;
  private connected: boolean = false;

  async connect(): Promise<boolean> {
    if (this.connected && this.channel) return true;

    try {
      console.log(`⌛️ URL: amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`);
      console.log(`⌛️ Connecting to Rabbit-MQ Server`);

      // this.connection = await client.connect(
      //   `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWORD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`
      //   , "heartbeat=60");


        this.connection = await client.connect(
          `amqp://admin:NTH@13042000@localhost:5672`
          , "heartbeat=60");

      console.log(`✅ Rabbit MQ Connection is ready`);

      this.channel = await this.connection.createChannel();

      console.log(`🛸 Created RabbitMQ Channel successfully`);

      this.connected = true;
      return true;
    } catch (error) {
      console.error(error);
      console.error(`Not connected to MQ Server`);
      return false;
    }
  }

  async sendToQueue(queue: string, message: any) {
    try {
      if (!this.connected || !this.channel) {
        const isConnected = await this.connect();
        if (!isConnected) {
          throw new Error('Failed to connect to RabbitMQ');
        }
      }

      this.channel.sendToQueue(queue, Buffer.from(JSON.stringify(message)));
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async consume(handleIncomingNotification: HandlerCB) {
    try {
      if (!this.connected || !this.channel) {
        const isConnected = await this.connect();
        if (!isConnected) {
          throw new Error('Failed to connect to RabbitMQ');
        }
      }

      await this.channel.assertQueue(process.env.RABBITMQ_NOTIFICATION_QUEUE ?? 'test', {
        durable: true,
      });

      this.channel.consume(
        process.env.RABBITMQ_NOTIFICATION_QUEUE ?? 'test',
        (msg) => {
          {
            if (!msg) {
              return console.error(`Invalid incoming message`);
            }
            handleIncomingNotification(msg?.content?.toString());
            this.channel.ack(msg);
          }
        },
        {
          noAck: false,
        }
      );
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

}


export const mqConnection = new RabbitMQConnection();

