import  { Browser } from "puppeteer"; 

import { IProfile } from '../interfaces/index';

 export class BrowserWrapper {
    private _browser: Browser | null = null;
    private _profile: IProfile | null = null;

    setBrowser(newBrowser: Browser) {
        this._browser = newBrowser;
        // this._profile = profile;
    }

    getBrowser(): Browser | null {
        return this._browser;
    }
   

    getProfile(): IProfile | null {
        return this._profile;
    }

    async quit() {
        if (this._browser !== null) {
            await this._browser.close();
            this._browser = null;
        }
    }
}
