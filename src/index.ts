import { CrawlHoSoCongTy, CrawlDetailHoSoCongTy, CrawlHoSoCongTyNewConcurrency } from "./constant/index";
// import * as rabbitMq from "./entity/models/index";
// import {sendNotification}  from "./entity/interfaces/index";
import * as readline from 'readline/promises';
import axios, { AxiosError, AxiosProxyConfig } from "axios";
import UserAgent from 'user-agents';
import * as https from 'https';
import { HttpsProxyAgent } from 'https-proxy-agent';
import * as http from 'http';
import { HttpProxyAgent } from 'http-proxy-agent';
const agent = new HttpProxyAgent('http://203.57.51.53:80');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
(async () => {
    try {
       
        // https.get('https://hosocongty.vn', { agent ,rejectUnauthorized:false}, (res) => {
        //     console.log('"response" event!', res);
        //     res.pipe(process.stdout);
        //   });
        // const axios_instance = await setupAxiosWithProxy();
        // axios_instance.get('http://hosocongty.vn/', {
            
        // }).then(response => {
        //     console.log(response.data);
        // }).catch(error => {
        //     console.error('Error:', error);
        // });

          await CrawlHoSoCongTyNewConcurrency();

        // let answer;
        // do {
        //     console.log(`---------------------------CRAWLER HOME---------------------------`)
        //     console.log(`1.CrawlHoSoCongTy`)
        //     console.log(`2.CrawlDetailHoSoCongTy`)
        //     console.log(`3.Connection Mongo`)

        //     console.log(`4.Exit`)

        //     answer = await rl.question('You choice? [0] ', {
        //         signal: AbortSignal.timeout(50_000) // 10s timeout
        //     });
        //     switch (answer.toLowerCase()) {
        //         case '1':
        //             console.log('Super!');
        //             break;
        //         case '2':
        //             await CrawlHoSoCongTyNewConcurrency();
        //             break;
        //         case '3':
        //             // await insertCompanyDatasAsync();
        //             break;
        //         case '4':
        //             break;
        //         default:
        //             console.log('Please Choice...');
        //     }
        // } while (answer !== '3');


    } catch (error) {
        console.error("Error:", error);
    } finally {
        rl.close();
    }
})();



async function setupAxiosWithProxy() {

    const userAgent = new UserAgent();
    let proxyConfig: AxiosProxyConfig = {
        host: '35.79.120.242',
        port: 3128,
        protocol: 'http'
    };

    const httpAgent = new http.Agent({
        keepAlive: true,
        maxSockets: Infinity,
        // rejectUnauthorized: false

    });

    const axios_instance = axios.create({
        httpAgent: httpAgent,
        headers: {
            'Cookie': '',
            'User-Agent': `${userAgent.toString()}`,
            'Accept': "*/*",
        },
        proxy: proxyConfig
    });

    // Add retry logic to the axios instance
    axios_instance.interceptors.response.use(undefined, (error: AxiosError) => {
        const { config, response } = error;
        const isAxiosError = error.isAxiosError || false;

        if (isAxiosError && response && response.status === 429) { // Retry if status code is 429 (Too Many Requests)
            return new Promise((resolve) => {
                if (config) {
                    setTimeout(() => resolve(axios_instance.request(config)), 10000); // Retry after 1 second
                }
            });

        }

        if (error.code === 'ECONNRESET') { // Retry if connection reset
            return new Promise((resolve) => {
                if (config) {
                    setTimeout(() => resolve(axios_instance.request(config)), 10000); // Retry after 1 second

                }
            });
        }

        return Promise.reject(error);
    });

    return axios_instance;
}
