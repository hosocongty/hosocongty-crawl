-- CreateTable
CREATE TABLE "Company" (
    "id" TEXT NOT NULL,
    "codeTax" TEXT NOT NULL,
    "name" TEXT,
    "legalRepresentative" TEXT,
    "sourceData" TEXT,
    "slug" TEXT,
    "isActive" BOOLEAN NOT NULL DEFAULT true,
    "companyAttributeValueId" TEXT,

    CONSTRAINT "Company_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CompanyAttribute" (
    "id" TEXT NOT NULL,
    "name" TEXT,
    "type" TEXT,
    "order" INTEGER NOT NULL DEFAULT 0,
    "isShowUI" BOOLEAN NOT NULL DEFAULT false,
    "isActive" BOOLEAN NOT NULL DEFAULT true,
    "companyAttributeValueId" TEXT,

    CONSTRAINT "CompanyAttribute_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CompanyAttributeValue" (
    "id" TEXT NOT NULL,
    "companyId" TEXT,
    "companyAttributeId" TEXT,
    "value" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT true,

    CONSTRAINT "CompanyAttributeValue_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CompanyBusinessActivities" (
    "id" TEXT NOT NULL,
    "companyId" TEXT NOT NULL,
    "businessActivitieId" TEXT NOT NULL,

    CONSTRAINT "CompanyBusinessActivities_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "BusinessActivities" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "level" INTEGER NOT NULL,
    "slug" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT true,
    "isDelete" BOOLEAN NOT NULL DEFAULT false,
    "parentBusinessActivitieId" TEXT NOT NULL,

    CONSTRAINT "BusinessActivities_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CompanyTags" (
    "id" TEXT NOT NULL,
    "companyId" TEXT NOT NULL,
    "tagId" TEXT NOT NULL,

    CONSTRAINT "CompanyTags_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Tags" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "slug" TEXT NOT NULL,
    "isActive" BOOLEAN NOT NULL DEFAULT true,

    CONSTRAINT "Tags_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Company_codeTax_key" ON "Company"("codeTax");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyAttributeValue_companyId_key" ON "CompanyAttributeValue"("companyId");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyAttributeValue_companyAttributeId_key" ON "CompanyAttributeValue"("companyAttributeId");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyBusinessActivities_companyId_key" ON "CompanyBusinessActivities"("companyId");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyBusinessActivities_businessActivitieId_key" ON "CompanyBusinessActivities"("businessActivitieId");

-- CreateIndex
CREATE UNIQUE INDEX "BusinessActivities_level_key" ON "BusinessActivities"("level");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyTags_companyId_key" ON "CompanyTags"("companyId");

-- CreateIndex
CREATE UNIQUE INDEX "CompanyTags_tagId_key" ON "CompanyTags"("tagId");

-- AddForeignKey
ALTER TABLE "Company" ADD CONSTRAINT "Company_companyAttributeValueId_fkey" FOREIGN KEY ("companyAttributeValueId") REFERENCES "CompanyAttributeValue"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CompanyAttribute" ADD CONSTRAINT "CompanyAttribute_companyAttributeValueId_fkey" FOREIGN KEY ("companyAttributeValueId") REFERENCES "CompanyAttributeValue"("id") ON DELETE SET NULL ON UPDATE CASCADE;
