-- CreateTable
CREATE TABLE "CrawlHoSoCongTy" (
    "id" SERIAL NOT NULL,
    "link" TEXT NOT NULL,
    "nameCompany" TEXT NOT NULL,
    "isCrawl" BOOLEAN NOT NULL,

    CONSTRAINT "CrawlHoSoCongTy_pkey" PRIMARY KEY ("id")
);
