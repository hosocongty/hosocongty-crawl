-- CreateTable
CREATE TABLE "HoSoCongTy" (
    "id" TEXT NOT NULL,
    "json" JSONB[],
    "taxCode" TEXT NOT NULL,
    "page" INTEGER NOT NULL,

    CONSTRAINT "HoSoCongTy_pkey" PRIMARY KEY ("id")
);
