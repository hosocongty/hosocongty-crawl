/*
  Warnings:

  - Added the required column `page` to the `CrawlHoSoCongTy` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "CrawlHoSoCongTy" ADD COLUMN     "page" INTEGER NOT NULL;
